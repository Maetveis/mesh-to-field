#include <vector>

#include <cstdint>

extern const std::vector<std::uint32_t> vertex_shader_code;
extern const std::vector<std::uint32_t> fragment_shader_code;