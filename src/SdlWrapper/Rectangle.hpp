#pragma once

#include "Point.hpp"
#include "Vector.hpp"

namespace SDL {

struct Rectangle {
    Point  top_left_corner;
    Vector extents;

    constexpr Point bottom_right_corner() const
    {
        return top_left_corner + extents;
    }

    friend constexpr bool operator==(const Rectangle lhs, const Rectangle rhs)
    {
        return lhs.top_left_corner == rhs.top_left_corner && lhs.extents == rhs.extents;
    }

    constexpr bool contains(const Point p) const
    {
        return p >= top_left_corner && p < bottom_right_corner();
    }

    constexpr bool contains(const Rectangle other) const
    {
        return contains(other.top_left_corner) && contains(other.bottom_right_corner());
    }

    constexpr bool overlapsWith(const Rectangle other) const
    {
        return contains(other.top_left_corner) || contains(other.bottom_right_corner());
    }

    explicit constexpr operator bool()
    {
        return extents >= Vector{0, 0};
    }
};

} // namespace SDL