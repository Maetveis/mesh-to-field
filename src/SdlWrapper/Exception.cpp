#include "Exception.hpp"

#include <SDL_error.h>

namespace SDL {

Exception::Exception()
    : std::runtime_error(SDL_GetError()){};

} // namespace SDL