#pragma once

#include <stdexcept>

namespace SDL {

struct Exception : std::runtime_error {
    using std::runtime_error::runtime_error;

    Exception();
    Exception(const Exception&) = default;
    Exception& operator=(const Exception&) = default;
};

} // namespace SDL