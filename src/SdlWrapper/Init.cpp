#include "Init.hpp"

#include "Exception.hpp"

#include <SDL.h>

namespace SDL {

Init::Init()
{
    if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_EVENTS | SDL_INIT_VIDEO) < 0) {
        throw InitError{};
    }
}

Init::~Init()
{
    SDL_Quit();
}

InitError::InitError()
    : Exception(std::string{"SDL_Init Failed: "} + SDL_GetError())
{
}

};