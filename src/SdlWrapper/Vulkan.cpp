#include "Vulkan.hpp"

#include "Window.hpp"

#include <SDL_vulkan.h>

#include <fmt/format.h>
#include <spdlog/spdlog.h>

namespace SDL {

std::vector<const char*> vulkanRequiredExtensions(Window& window)
{
    unsigned int count = 0;
    if (!SDL_Vulkan_GetInstanceExtensions(window, &count, nullptr)) {
        throw VulkanError{};
    }

    std::vector<const char*> extensions(count, nullptr);

    if (!SDL_Vulkan_GetInstanceExtensions(window, &count, extensions.data())) {
        throw VulkanError{};
    }

    spdlog::info("Extensions required for SDL vulkan support: {}",
                 fmt::join(extensions.begin(), extensions.end(), ", "));

    return extensions;
}

} // namespace SDL