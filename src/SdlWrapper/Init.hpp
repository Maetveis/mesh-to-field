#pragma once

#include "Exception.hpp"

namespace SDL {

/**
 * \brief Initialize SDL
 * This is a RAII object for initializing and destroying SDL
 */
struct Init {
    /**
     * \brief Initialize SDL
     * \throw InitError on failure
     */
    Init();

    /**
     * \brief Destroy the SDL contex
     */
    ~Init();
};

struct InitError : Exception {
    InitError();
    InitError(const InitError&) = default;
    InitError& operator=(const InitError&) = default;
};

} // namespace SDL