#pragma once

#include <functional>

namespace SDL {

struct Vector {
    int x;
    int y;

    template <typename T>
    friend constexpr Vector operator*(const Vector lhs, const T scale)
    {
        return {scale * lhs.x, scale * lhs.y};
    }

    template <typename T>
    friend constexpr Vector operator*(const T scale, const Vector rhs)
    {
        return rhs * scale;
    }

    template <typename T>
    constexpr Vector& operator*=(const T scale)
    {
        return *this = scale * this;
    }

    friend constexpr Vector operator+(const Vector lhs, const Vector rhs)
    {
        return {lhs.x + rhs.x, lhs.y + rhs.y};
    }

    constexpr Vector& operator+=(const Vector rhs)
    {
        return *this = *this + rhs;
    }

    friend constexpr Vector operator-(const Vector lhs, const Vector rhs)
    {
        return {lhs.x - rhs.x, lhs.y - rhs.y};
    }

    constexpr Vector& operator-=(const Vector rhs)
    {
        return *this = *this - rhs;
    }

    friend constexpr bool operator==(const Vector lhs, const Vector rhs)
    {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }

    friend constexpr bool operator<(const Vector lhs, const Vector rhs)
    {
        return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y);
    }

    friend constexpr bool operator>(const Vector lhs, const Vector rhs)
    {
        return rhs < lhs;
    }

    friend constexpr bool operator<=(const Vector lhs, const Vector rhs)
    {
        return !(lhs > rhs);
    }

    friend constexpr bool operator>=(const Vector lhs, const Vector rhs)
    {
        return !(lhs < rhs);
    }

    friend struct std::hash<Vector>;
};

} // namespace SDL

namespace std {

template <>
struct hash<SDL::Vector> {
    std::size_t operator()(const SDL::Vector v) const
    {
        return std::hash<decltype(v.x)>{}(v.x) ^ (std::hash<decltype(v.x)>{}(v.y) << 1);
    }
};

} // namespace std
