#pragma once

#include "Exception.hpp"

#include <vector>

namespace SDL {

class Window;

struct VulkanError : Exception {
    using Exception::Exception;
};

/**
 * \brief Get the required extensions for vulcan support from SDL
 * \param window a window created with vulkan support enabled
 * \return the required extensions
 * \throw VulkanError upon failure
 */
std::vector<const char*> vulkanRequiredExtensions(Window& window);

} // namespace SDL