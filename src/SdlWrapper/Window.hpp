#pragma once

#include "Exception.hpp"
#include "Rectangle.hpp"

#include <vulkan/vulkan.hpp>

#include <memory>

#include <cstdint>

// Forward declarations
struct SDL_Window;

namespace vk {
class Instance;
class SurfaceKHR;
}

namespace SDL {

/**
 * \brief Backend Driver for the window's framebuffer
 */
enum class VideoBackend {
    None,   //< Default SDL surface window
    OpenGL, //< OpenGL capable window
    Vulkan  //< Vulkan capable window
};

/**
 * \brief A Wrapper RAII class around SDL Window and related function
 * \sa SDL_Window
 */
class Window {
public:
    ///** Window Type */
    // enum class Type {
    //    Windowed,          //< Window with decorations
    //    Borderless,        //< Borderless window
    //    FullScreen,        //< FullScreen window
    //    DesktopFullScreen  //< Borderless FullScreen "fake fullscreen according to SDL"
    //};

    /** Window State */
    enum class State {
        Restored,  //< Window visible, not minimized or maximized
        Minimized, //< Window is maximized
        Maximized  //< Window is minimized
    };

    /**
     * \brief Parameters for windowed (normal) window creation
     */
    struct WindowedParams {
        Rectangle position;                      //<! position of the window
        bool      decorations = true;            //<! draw window decorations (on or off)
        bool      resizable   = false;           //<! allow resizing
        State     state       = State::Restored; //<! initial state of the window
    };

    /**
     * \brief Parameters for fullscreen window creation
     */
    struct FullScreenParams {
        /** Use "real" fullscreen mode if false, borderless fullscreen window if true */
        bool borderless = false;
    };

    Window(SDL_Window* window)
        : window_(window)
    {
    }

    /**
     * \brief Default constructor, no window is created
     */
    Window() = default;

    /**
     * \brief Create a window
     * \note calls create
     * \sa create
     */
    Window(const char* title, Rectangle position, std::uint32_t flags)
        : Window{}
    {
        create(title, position, flags);
    }

    /**
     * \brief Create a windowed window
     * \note calls createWindowed
     * \sa createWindowed
     */
    Window(const char* title, WindowedParams params, VideoBackend backend = VideoBackend::None,
           bool hidden = false)
        : Window{}
    {
        createWindowed(title, params, backend, hidden);
    }

    /**
     * \brief Create a fullscreen window
     * \note calls createFullScreen
     * \sa createFullScreen
     */
    Window(const char* title, FullScreenParams params, VideoBackend backend = VideoBackend::None,
           bool hidden = false)
        : Window{}
    {
        createFullScreen(title, params, backend, hidden);
    }

    /**
     * \brief Create a window
     * \param title title of the window
     * \param position position and size of the window
     * \param flags additional flags to use at window creation
     *
     * \throw WindowError if SDL fails to create the window
     * \sa SDL_CreateWindow
     */
    Window& create(const char* title, Rectangle position, std::uint32_t flags);

    /**
     * \brief Create a windowed or borderless window
     * \param title   title of the window
     * \param params  initial parameters for the window \see WindowedParams
     * \param hidden  set to true to hide the window initially
     * \param backend video backend for the framebuffer of the window
     *
     * \throw WindowError if SDL fails to create the window
     * \sa SDL_CreateWindow
     */
    Window& createWindowed(const char* title, WindowedParams params,
                           VideoBackend backend = VideoBackend::None, bool hidden = false);

    /**
     * \brief Create a full screen window
     * \param title   title of the window
     * \param params  initial parameters for the window \see FullScreenParams
     * \param hidden  set to true to hide initally
     * \param backend video backend for the framebuffer of the window
     *
     * \throw WindowError if SDL fails to create the window
     * \sa SDL_CreateWindow
     */
    Window& createFullScreen(const char* title, FullScreenParams params,
                             VideoBackend backend = VideoBackend::None, bool hidden = false);

    /**
     * \brief Free the SDL resource owned by this window (if any)
     * After this call no window is attached this object
     */
    void destroy()
    {
        window_.reset();
    }

    /**
     * Get the underlying SDL_Window pointer
     * \return the SDL window pointer
     */
    SDL_Window* get()
    {
        return window_.get();
    }

    /**
     * Get the underlying SDL_Window pointer
     * \return the SDL window pointer
     */
    const SDL_Window* get() const
    {
        return window_.get();
    }

    /**
     * \brief Conversion to SDL_Window*
     */
    operator SDL_Window*()
    {
        return get();
    }

    /**
     * \brief Conversion to const SDL_Window*
     */
    operator const SDL_Window*() const
    {
        return get();
    }

    /**
     * \brief Create a Vulkan Surface for the window
     */
    vk::UniqueSurfaceKHR createVkSurface(vk::Instance instance);

private:
    /** Deleter for the unique_ptr calls SDL_DestroyWindow */
    struct Deleter {
        void operator()(SDL_Window* window) const;
    };
    std::unique_ptr<SDL_Window, Deleter> window_; //<! Wrapped SDL_Window pointer
};

struct WindowError : Exception {
    using Exception::Exception;
};

} // namespace SDL