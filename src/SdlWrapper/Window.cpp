#include "Window.hpp"

#include "Rectangle.hpp"
#include "Vulkan.hpp"

#include <SDL.h>
#include <SDL_video.h>
#include <SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

namespace SDL {

Window& Window::create(const char* title, const Rectangle position, const std::uint32_t flags)
{
    window_ = std::unique_ptr<SDL_Window, Deleter>{
        SDL_CreateWindow(title, position.top_left_corner.x, position.top_left_corner.y,
                         position.extents.x, position.extents.y, flags)};

    if (!window_) {
        throw WindowError{};
    }

    return *this;
}

Window& Window::createWindowed(const char* title, WindowedParams params, VideoBackend backend,
                               bool hidden)
{
    std::uint32_t flags = params.decorations ? 0 : SDL_WINDOW_BORDERLESS;
    flags |= params.resizable ? SDL_WINDOW_RESIZABLE : 0;
    flags |= [&]() -> std::uint32_t {
        switch (params.state) {
        case State::Minimized:
            return SDL_WINDOW_MINIMIZED;
        case State::Maximized:
            return SDL_WINDOW_MAXIMIZED;
        default:
            return 0;
        }
    }();
    flags |= hidden ? SDL_WINDOW_HIDDEN : 0;
    flags |= [&]() -> std::uint32_t {
        switch (backend) {
        case VideoBackend::OpenGL:
            return SDL_WINDOW_OPENGL;
        case VideoBackend::Vulkan:
            return SDL_WINDOW_VULKAN;
        default:
            return 0;
        }
    }();

    return create(title, params.position, flags);
}

Window& Window::createFullScreen(const char* title, FullScreenParams params, VideoBackend backend,
                                 bool hidden)
{
    std::uint32_t flags = params.borderless ? SDL_WINDOW_FULLSCREEN_DESKTOP : SDL_WINDOW_FULLSCREEN;

    flags |= hidden ? SDL_WINDOW_HIDDEN : 0;
    flags |= [&]() -> std::uint32_t {
        switch (backend) {
        case VideoBackend::OpenGL:
            return SDL_WINDOW_OPENGL;
        case VideoBackend::Vulkan:
            return SDL_WINDOW_VULKAN;
        default:
            return 0;
        }
    }();

    return create(title, Rectangle{}, flags);
}

vk::UniqueSurfaceKHR Window::createVkSurface(vk::Instance instance)
{
    VkSurfaceKHR surface;
    if (!SDL_Vulkan_CreateSurface(window_.get(), instance, &surface)) {
        throw WindowError{};
    }
    return vk::UniqueSurfaceKHR{vk::SurfaceKHR{surface}, instance};
}

void Window::Deleter::operator()(SDL_Window* ptr) const
{
    SDL_DestroyWindow(ptr);
}

} // namespace SDL