#pragma once

#include "Vector.hpp"

#include <functional>

namespace SDL {

struct Point {
    int x;
    int y;

    friend constexpr Point operator+(const Point p, const Vector v)
    {
        return {p.x + v.x, p.y + v.y};
    }

    constexpr Point& operator+=(const Vector v)
    {
        return *this = *this + v;
    }

    friend constexpr Point operator+(const Vector v, const Point p)
    {
        return p + v;
    }

    friend constexpr Point operator-(const Point p, const Vector v)
    {
        return {p.x - v.x, p.y - v.y};
    }

    constexpr Point& operator-=(const Vector v)
    {
        return *this = *this - v;
    }

    friend constexpr Point operator-(const Point v, const Point p)
    {
        return p - v;
    }

    friend constexpr bool operator==(const Point lhs, const Point rhs)
    {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }

    friend constexpr bool operator<(const Point lhs, const Point rhs)
    {
        return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y);
    }

    friend constexpr bool operator>(const Point lhs, const Point rhs)
    {
        return rhs < lhs;
    }

    friend constexpr bool operator<=(const Point lhs, const Point rhs)
    {
        return !(lhs > rhs);
    }

    friend constexpr bool operator>=(const Point lhs, const Point rhs)
    {
        return !(lhs < rhs);
    }

    friend struct std::hash<Point>;
};

} // namespace SDL

namespace std {
template <>
struct hash<SDL::Point> {
    std::size_t operator()(const SDL::Point v) const
    {
        return std::hash<decltype(v.x)>{}(v.x) ^ (std::hash<decltype(v.x)>{}(v.y) << 1);
    }
};
} // namespace SDL