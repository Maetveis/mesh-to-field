#pragma once

#include "SdlWrapper/Init.hpp"
#include "SdlWrapper/Vulkan.hpp"
#include "SdlWrapper/Window.hpp"
#include "SetupInstance.hpp"
#include "Triangle.hpp"
#include "VulkanHelpers/FrameQueue.hpp"
#include "VulkanHelpers/Helpers.hpp"

#include <SDL_video.h>

#include <vulkan/vulkan.hpp>

class Game {
public:
    Game();
    void run();
    ~Game();

private:
    struct SetupParams;

    Game(SetupParams);
    void handleEvents();
    void update();
    void draw();

    [[no_unique_address]] SDL::Init init; //<! Initialize SDL upon construction
    /** Main Window */
    SDL::Window        window_{"Vulkan Hello Triangle",
                        {{SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 768}},
                        SDL::VideoBackend::Vulkan};
    vk::UniqueInstance instance_ = setupInstance(window_); //<! the global vulkan instance
#ifdef VULKAN_VALIDATION_LAYERS
    vk::DebugUtilsMessengerEXT debug_messenger_;
#endif
    vk::UniqueDevice   device_;
    Vulkan::FrameQueue frame_queue_;
    Triangle           triangle_;
    bool               running_ = true; //<! Continue running, exits if false
};