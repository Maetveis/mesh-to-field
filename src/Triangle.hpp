#pragma once

#include "Triangle.shaders.hpp"
#include "VulkanHelpers/FrameQueue.hpp"

#include <vulkan/vulkan.hpp>

class Triangle {
public:
    Triangle(vk::Device device, const Vulkan::FrameQueue& frame_queue,
             const Vulkan::PresentSupport& present_info)
        : render_pass_{createRenderPass(device, frame_queue, present_info)}
        , pipeline_layout_{device.createPipelineLayoutUnique({})}
        , pipeline_{createPipeLine(device, present_info)}
        , framebuffers_{present_info.num_images}
        , command_pool_{device.createCommandPoolUnique(
              {vk::CommandPoolCreateFlagBits::eTransient
                   | vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
               present_info.graphics_present_family})}
        , queue_{device.getQueue(0, present_info.graphics_present_family)}
        , command_buffers_{device.allocateCommandBuffersUnique({command_pool_.get(),
                                                                vk::CommandBufferLevel::ePrimary,
                                                                present_info.frames_in_flight})}
        , render_area(present_info.surface_extent)
    {
        std::uint32_t image_id = 0;
        for (auto& framebuffer : framebuffers_) {
            framebuffer = device.createFramebufferUnique(vk::FramebufferCreateInfo{
                {},                                    // flags
                render_pass_.get(),                    // renderpass
                1,                                     // number of attachments
                &frame_queue.getImageView(image_id++), // attachments
                present_info.surface_extent.width,     // width
                present_info.surface_extent.height,    // height
                1                                      // layers
            });
        }
    }

    void draw(const Vulkan::FramePromise& promise)
    {
        auto& command_buffer = command_buffers_[promise.frame_id];
        auto& framebuffer    = framebuffers_[promise.image_id];

        command_buffer->reset({});
        command_buffer->begin(vk::CommandBufferBeginInfo{});

        const auto clear_values = {vk::ClearValue{{std::array{0.f, 0.f, 0.f, 1.f}}}};

        command_buffer->beginRenderPass(
            {render_pass_.get(), framebuffer.get(), {{0, 0}, render_area}, clear_values},
            vk::SubpassContents::eInline);

        command_buffer->bindPipeline(vk::PipelineBindPoint::eGraphics, pipeline_.get());
        command_buffer->draw(3, 1, 0, 0);

        command_buffer->endRenderPass();
        command_buffer->end();

        const vk::PipelineStageFlags wait_stage_mask
            = vk::PipelineStageFlagBits::eColorAttachmentOutput;
        const auto submit_infos
            = {vk::SubmitInfo{1, &promise.image_acquired, &wait_stage_mask, 1,
                              &command_buffer.get(), 1, &promise.ready_to_present}};

        queue_.submit(submit_infos, promise.command_finished);
    }

private:
    static vk::UniqueRenderPass createRenderPass(vk::Device                    device,
                                                 const Vulkan::FrameQueue&     frame_queue,
                                                 const Vulkan::PresentSupport& present_info)
    {
        const auto renderpass_attachments
            = {vk::AttachmentDescription{{},
                                         present_info.surface_format.format,
                                         vk::SampleCountFlagBits::e1,
                                         vk::AttachmentLoadOp::eClear,
                                         vk::AttachmentStoreOp::eStore,
                                         vk::AttachmentLoadOp::eDontCare,
                                         vk::AttachmentStoreOp::eDontCare,
                                         vk::ImageLayout::eUndefined,
                                         vk::ImageLayout::ePresentSrcKHR}};

        const auto subpass_color_attachments = {
            vk::AttachmentReference{0, vk::ImageLayout::eColorAttachmentOptimal},
        };

        const auto subpasses = {vk::SubpassDescription{
            {},                               // flags
            vk::PipelineBindPoint::eGraphics, // bindpoint
            {},                               // input attachments
            subpass_color_attachments,        // output attachments
        }};

        const auto dependencies = {vk::SubpassDependency{
            VK_SUBPASS_EXTERNAL,                               // src subpass
            0,                                                 // dest subpass
            vk::PipelineStageFlagBits::eColorAttachmentOutput, // src stage
            vk::PipelineStageFlagBits::eColorAttachmentOutput, // dst stage
            vk::AccessFlags{},                                 // src access flags
            vk::AccessFlagBits::eColorAttachmentWrite // dst access flags I dont get this at all
        }};

        const auto render_pass_info
            = vk::RenderPassCreateInfo{{}, renderpass_attachments, subpasses, dependencies};

        return device.createRenderPassUnique({{}, renderpass_attachments, subpasses, dependencies});
    }

    vk::UniquePipeline createPipeLine(vk::Device device, const Vulkan::PresentSupport& present_info)
    {

        const auto vertex_shader_module = device.createShaderModuleUnique({{}, vertex_shader_code});

        const auto fragment_shader_module
            = device.createShaderModuleUnique({{}, fragment_shader_code});

        const std::initializer_list<vk::PipelineShaderStageCreateInfo> shader_create_infos
            = {{{}, vk::ShaderStageFlagBits::eVertex, vertex_shader_module.get(), "main"},
               {{}, vk::ShaderStageFlagBits::eFragment, fragment_shader_module.get(), "main"}};

        const vk::PipelineVertexInputStateCreateInfo   vertex_input{};
        const vk::PipelineInputAssemblyStateCreateInfo input_assembly{
            {}, vk::PrimitiveTopology::eTriangleList};
        const vk::PipelineTessellationStateCreateInfo tessalation{};

        const auto viewports = {vk::Viewport{
            0.f,                                                     // origin x
            static_cast<float>(present_info.surface_extent.height),  // origin y
            static_cast<float>(present_info.surface_extent.width),   // width
            -static_cast<float>(present_info.surface_extent.height), // height
            0.f,                                                     // mindepth
            1.f                                                      // maxdepth
        }};

        const auto scissors = {vk::Rect2D{{0, 0}, present_info.surface_extent}};

        const vk::PipelineViewportStateCreateInfo viewport_state = {{}, viewports, scissors};

        vk::PipelineRasterizationStateCreateInfo rasterization{{},
                                                               false,
                                                               false,
                                                               vk::PolygonMode::eFill,
                                                               vk::CullModeFlagBits::eBack,
                                                               vk::FrontFace::eCounterClockwise};
        rasterization.setLineWidth(1.f);

        const vk::PipelineMultisampleStateCreateInfo multisample{{}, vk::SampleCountFlagBits::e1};

        using colorbit = vk::ColorComponentFlagBits;

        auto attachments = std::array{vk::PipelineColorBlendAttachmentState{false}};
        std::begin(attachments)->colorWriteMask = colorbit::eR | colorbit::eG | colorbit::eB;

        const vk::PipelineColorBlendStateCreateInfo color_blending{
            {}, false, vk::LogicOp::eCopy, attachments};

        return device
            .createGraphicsPipelineUnique(
                {},
                vk::GraphicsPipelineCreateInfo{
                    {},                  // flags
                    shader_create_infos, // shader stages
                    &vertex_input,   // vertex input state (No vertex input or descriptors in this
                                     // shader)
                    &input_assembly, // vertex input assembly state
                    &tessalation,    // tessalation state
                    &viewport_state, // viewport
                    &rasterization,  // rasterization
                    &multisample,    // multisample
                    nullptr,         // depth/stencil test
                    &color_blending, // color_blending
                    nullptr,         // dynamic viewport or state is not used
                    pipeline_layout_.get(), // Pipeline layout (uniforms, textures, push constants
                                            // (descriptors))
                    render_pass_.get(),     // render pass
                    0,                      // subpass
                })
            .value;
    }

    vk::UniqueRenderPass               render_pass_;
    vk::UniquePipelineLayout           pipeline_layout_;
    vk::UniquePipeline                 pipeline_;
    std::vector<vk::UniqueFramebuffer> framebuffers_;

    vk::UniqueCommandPool command_pool_;
    vk::Queue             queue_;

    std::vector<vk::UniqueCommandBuffer> command_buffers_;
    const vk::Extent2D                   render_area;
};