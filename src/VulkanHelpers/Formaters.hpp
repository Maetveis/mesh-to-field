#pragma once

#include <fmt/core.h>
#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_core.h>

#include <type_traits>

namespace Vulkan {

/**
 * \brief Helper struct for formatting vulkan version
 */
struct VersionInfo {
    std::uint32_t version_info;

    std::uint32_t major() const
    {
        return VK_VERSION_MAJOR(version_info);
    }
    std::uint32_t minor() const
    {
        return VK_VERSION_MINOR(version_info);
    }
    std::uint32_t patch() const
    {
        return VK_VERSION_PATCH(version_info);
    }
};

} // namespace Vulkan

/**
 * \brief Formatter for any vulkan type which has a vk::to_sting method
 */
template <typename VulkanType_>
struct fmt::formatter<
    VulkanType_, char,
    std::enable_if_t<std::is_same_v<decltype(vk::to_string(VulkanType_{})), std::string>>>
    : fmt::formatter<string_view> {
    template <typename FormatCtx>
    auto format(const VulkanType_& value, FormatCtx& ctx)
    {
        return formatter<string_view>::format(vk::to_string(value), ctx);
    }
};

/**
 * \brief Formatter for Vulkan::VersionInfo
 */
template <>
struct fmt::formatter<Vulkan::VersionInfo> {
    constexpr auto parse(fmt::format_parse_context& contex)
    {
        if (contex.begin() != contex.end() && *contex.begin() != '}') {
            throw fmt::format_error("invalid Format");
        }
        return contex.begin();
    }

    template <typename FormatCtx>
    auto format(const Vulkan::VersionInfo& version, FormatCtx& ctx)
    {
        return fmt::format_to(ctx.out(), "{}.{}.{}", version.major(), version.minor(),
                              version.patch());
    }
};

/**
 * \brief Formatter for vk::ExtensionProperties
 */
template <>
struct fmt::formatter<vk::ExtensionProperties> {
    constexpr auto parse(fmt::format_parse_context& contex)
    {
        if (contex.begin() != contex.end() && *contex.begin() != '}') {
            throw fmt::format_error("invalid Format");
        }
        return contex.begin();
    }

    template <typename FormatCtx>
    auto format(const vk::ExtensionProperties& ext_properties, FormatCtx& ctx)
    {
        return fmt::format_to(ctx.out(), "({}: {})", ext_properties.extensionName,
                              ext_properties.specVersion);
    }
};

/**
 * \brief Formatter for vk::LayerProperties
 */
template <>
struct fmt::formatter<vk::LayerProperties> {
    constexpr auto parse(fmt::format_parse_context& contex)
    {
        if (contex.begin() != contex.end() && *contex.begin() != '}') {
            throw fmt::format_error("invalid Format");
        }
        return contex.begin();
    }

    template <typename FormatCtx>
    auto format(const vk::LayerProperties& layer_properties, FormatCtx& ctx)
    {
        return fmt::format_to(ctx.out(), "({}: {} ({}, {}))", layer_properties.layerName,
                              layer_properties.description,
                              Vulkan::VersionInfo{layer_properties.specVersion},
                              layer_properties.implementationVersion);
    }
};

/**
 * \brief Formatter for vk::SurfaceFormatKHR
 */
template <>
struct fmt::formatter<vk::SurfaceFormatKHR> {
    constexpr auto parse(fmt::format_parse_context& contex)
    {
        if (contex.begin() != contex.end() && *contex.begin() != '}') {
            throw fmt::format_error("invalid Format");
        }
        return contex.begin();
    }

    template <typename FormatCtx>
    auto format(const vk::SurfaceFormatKHR& format, FormatCtx& ctx)
    {
        return fmt::format_to(ctx.out(), "({}, {})", format.format, format.colorSpace);
    }
};
