#pragma once

#include "Formaters.hpp"

#include <fmt/format.h>
#include <spdlog/spdlog.h>
#include <vulkan/vulkan.hpp>

#include <algorithm>
#include <limits>
#include <optional>
#include <stdexcept>

namespace Vulkan {

struct PresentSupport {
    std::uint32_t        frames_in_flight;
    std::uint32_t        num_images;
    std::uint32_t        graphics_present_family;
    vk::SurfaceFormatKHR surface_format;
    vk::Extent2D         surface_extent;
    vk::ImageUsageFlags  image_usage;
};

struct FramePromise {
    std::size_t   frame_id;
    std::size_t   image_id;
    vk::Semaphore image_acquired;
    vk::Semaphore ready_to_present;
    vk::Fence     command_finished;
};

class FrameQueue {
public:
    static std::optional<PresentSupport>
    getPresentSupport(vk::PhysicalDevice phy_device, vk::SurfaceKHR surface,
                      const std::uint32_t frames_in_flight = 2,
                      vk::ImageUsageFlags required_usage = vk::ImageUsageFlagBits::eColorAttachment)
    {
        const auto queue_families = phy_device.getQueueFamilyProperties();

        std::uint32_t graphics_present_family = queue_families.size();
        for (std::size_t index = 0; index < queue_families.size(); ++index) {
            if (queue_families[index].queueFlags & vk::QueueFlagBits::eGraphics
                && phy_device.getSurfaceSupportKHR(index, surface))
            {
                graphics_present_family = index;
            }
        }

        if (graphics_present_family == queue_families.size()) {
            spdlog::info("Couldn't find combined graphics and present family!");
            return {};
        }

        const auto capabilities = phy_device.getSurfaceCapabilitiesKHR(surface);
        if (!(capabilities.supportedUsageFlags & required_usage)
            || !(capabilities.supportedTransforms & vk::SurfaceTransformFlagBitsKHR::eIdentity)
            || !(capabilities.supportedCompositeAlpha & vk::CompositeAlphaFlagBitsKHR::eOpaque))
        {
            spdlog::info("Surface capabilities not sufficient!");
            return {};
        }

        if (capabilities.maxImageCount != 0
            && capabilities.minImageCount + frames_in_flight > capabilities.maxImageCount)
        {
            spdlog::info("Too few images supported in swapchain!");
            return {};
        }

        if (capabilities.currentExtent.width == 0xFF'FF'FF'FF) {
            spdlog::info("Surface has undefined size!");
            return {};
        }

        auto surface_formats = phy_device.getSurfaceFormatsKHR(surface);

        vk::SurfaceFormatKHR surface_format;
        if (const auto it = std::find(
                surface_formats.begin(), surface_formats.end(),
                vk::SurfaceFormatKHR{vk::Format::eB8G8R8Srgb, vk::ColorSpaceKHR::eSrgbNonlinear});
            it != surface_formats.end())
        {
            surface_format = *it;
        } else if (const auto it
                   = std::find(surface_formats.begin(), surface_formats.end(),
                               vk::SurfaceFormatKHR{vk::Format::eB8G8R8A8Srgb,
                                                    vk::ColorSpaceKHR::eSrgbNonlinear});
                   it != surface_formats.end())
        {
            surface_format = *it;
        } else {
            spdlog::info("No suitable image format found!");
            spdlog::info("Supported surface formats: {}",
                         fmt::join(surface_formats.begin(), surface_formats.end(), ", "));
            return {};
        }

        const auto num_images = capabilities.minImageCount + frames_in_flight;
        return PresentSupport{frames_in_flight,           num_images,
                              graphics_present_family,    surface_format,
                              capabilities.currentExtent, required_usage};
    };

    FrameQueue(vk::Device device, vk::UniqueSurfaceKHR surface,
               const PresentSupport& present_support)
        : device_{device}
        , present_queue_{device.getQueue(present_support.graphics_present_family, 0)}
        , surface_{std::move(surface)}
        , swapchain_{createSwapChain(device_, surface_.get(), present_support)}
        , frames_{present_support.frames_in_flight}
        , image_views_{present_support.num_images}
    {
        for (auto& frame : frames_) {
            frame.image_acquired   = device_.createSemaphoreUnique({});
            frame.ready_to_present = device_.createSemaphoreUnique({});
            frame.command_finished
                = device_.createFenceUnique({vk::FenceCreateFlagBits::eSignaled});
        }
        std::size_t image_index = 0;
        for (auto& image_view : image_views_) {
            image_view
                = device_.createImageViewUnique({{},
                                                 images_[image_index++],
                                                 vk::ImageViewType::e2D,
                                                 present_support.surface_format.format,
                                                 {
                                                     vk::ComponentSwizzle::eIdentity,
                                                     vk::ComponentSwizzle::eIdentity,
                                                     vk::ComponentSwizzle::eIdentity,
                                                     vk::ComponentSwizzle::eIdentity,
                                                 },
                                                 {vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1}});
        }
    }

    FramePromise startFrame()
    {
        constexpr auto max_timeout = std::numeric_limits<std::uint64_t>::max();

        switch (device_.waitForFences(frames_[current_frame_].command_finished.get(), true,
                                      max_timeout)) {
        case vk::Result::eSuccess:
            break;
        default:
            throw std::runtime_error("Waiting for fence failed");
        }
        device_.resetFences(frames_[current_frame_].command_finished.get());

        const auto result = device_.acquireNextImageKHR(
            swapchain_.get(), max_timeout, frames_[current_frame_].image_acquired.get(), {});
        switch (result.result) {
        case vk::Result::eSuccess:
        case vk::Result::eSuboptimalKHR:
            break;
        default:
            throw std::runtime_error(
                "acquireNextImageKHR returned error when trying to present image");
        }

        frames_[current_frame_].image_id = result.value;

        return {current_frame_, frames_[current_frame_].image_id,
                frames_[current_frame_].image_acquired.get(),
                frames_[current_frame_].ready_to_present.get(),
                frames_[current_frame_].command_finished.get()};
    }

    void submitFrame()
    {
        const auto wait_semaphores = {frames_[current_frame_].ready_to_present.get()};
        const auto swapchains      = {swapchain_.get()};
        const auto images          = {frames_[current_frame_].image_id};

        const auto result = present_queue_.presentKHR({wait_semaphores, swapchains, images});
        switch (result) {
        case vk::Result::eSuccess:
        case vk::Result::eSuboptimalKHR:
            break;
        default:
            throw std::runtime_error("presentKHR returned error when trying to present image");
        }

        current_frame_ = (current_frame_ + 1) % frames_.size();
    }

    auto numImages() const
    {
        return images_.size();
    };
    auto numFramesInFlight() const
    {
        return frames_.size();
    };

    const vk::ImageView& getImageView(std::uint32_t image_id) const
    {
        return image_views_[image_id].get();
    }

private:
    static vk::UniqueSwapchainKHR createSwapChain(vk::Device device, vk::SurfaceKHR surface,
                                                  const PresentSupport& present_support)
    {
        return device.createSwapchainKHRUnique({{},
                                                surface,
                                                present_support.num_images,
                                                present_support.surface_format.format,
                                                present_support.surface_format.colorSpace,
                                                present_support.surface_extent,
                                                1,
                                                vk::ImageUsageFlagBits::eColorAttachment,
                                                vk::SharingMode::eExclusive,
                                                {},
                                                vk::SurfaceTransformFlagBitsKHR::eIdentity,
                                                vk::CompositeAlphaFlagBitsKHR::eOpaque,
                                                vk::PresentModeKHR::eFifo,
                                                true});
    }

    vk::Device device_;
    vk::Queue  present_queue_;

    vk::UniqueSurfaceKHR   surface_;
    vk::UniqueSwapchainKHR swapchain_;

    std::vector<vk::Image>           images_ = device_.getSwapchainImagesKHR(swapchain_.get());
    std::vector<vk::UniqueImageView> image_views_;

    std::size_t current_frame_ = 0;
    struct FrameInFlight {
        std::uint32_t       image_id;
        vk::UniqueSemaphore image_acquired;
        vk::UniqueSemaphore ready_to_present;
        vk::UniqueFence     command_finished;
    };
    std::vector<FrameInFlight> frames_;
};

} // namespace Vulkan