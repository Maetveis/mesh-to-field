#pragma once

#include "Formaters.hpp"

#include <spdlog/spdlog.h>
#include <vulkan/vk_platform.h>
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_core.h>

#include <type_traits>

namespace Vulkan {

/**
 * \brief Set a function object as callback for DebugUtilsMessenger
 * \param callback the callback object
 * \param create_info the Vulkan create info for the debug utils
 */
template <typename Callback>
void set_debug_callback(Callback& callback, vk::DebugUtilsMessengerCreateInfoEXT& create_info)
{
    // Callback adapter function for vulkan
    struct VulkanCallback {
        static VKAPI_ATTR VkBool32 VKAPI_CALL
        call(VkDebugUtilsMessageSeverityFlagBitsEXT      message_severity,
             VkDebugUtilsMessageTypeFlagsEXT             message_type,
             const VkDebugUtilsMessengerCallbackDataEXT* callback_data, void* user_data)
        {
            vk::DebugUtilsMessageSeverityFlagBitsEXT      severity{message_severity};
            vk::DebugUtilsMessageTypeFlagBitsEXT          type{message_type};
            const vk::DebugUtilsMessengerCallbackDataEXT& data{*callback_data};

            if constexpr (std::is_convertible_v<decltype(callback(severity, type, data)), VkBool32>)
            {
                return static_cast<Callback*>(user_data)->callback(severity, type, data);
            } else {
                static_cast<Callback*>(user_data)->callback(severity, type, data);
                return false;
            }
        }
    };

    create_info.setPfnUserCallback(VulkanCallback::call);
    create_info.setPUserData(&callback);
}

/**
 * \brief Set a logger that logs debug messages to spdlog's default log
 * \param create_info the Vulkan create info for the debug utils
 */
inline void set_debug_logger(vk::DebugUtilsMessengerCreateInfoEXT& create_info)
{
    // Callback adapter function for vulkan
    struct Logger {
        static VKAPI_ATTR VkBool32 VKAPI_CALL
        call(VkDebugUtilsMessageSeverityFlagBitsEXT      message_severity,
             VkDebugUtilsMessageTypeFlagsEXT             message_type,
             const VkDebugUtilsMessengerCallbackDataEXT* callback_data, void* user_data)
        {
            vk::DebugUtilsMessageSeverityFlagBitsEXT      severity{message_severity};
            vk::DebugUtilsMessageTypeFlagBitsEXT          type{message_type};
            const vk::DebugUtilsMessengerCallbackDataEXT& data{*callback_data};

            using sevFlags  = vk::DebugUtilsMessageSeverityFlagBitsEXT;
            using typeFlags = vk::DebugUtilsMessageTypeFlagBitsEXT;

            constexpr const char* format = "Vulkan debug message [{}]: {}";

            switch (severity) {
            case sevFlags::eError:
                spdlog::error(format, type, data.pMessage);
                break;
            case sevFlags::eWarning:
                spdlog::warn(format, type, data.pMessage);
                break;
            case sevFlags::eInfo:
                spdlog::debug(format, type, data.pMessage);
                break;
            case sevFlags::eVerbose:
                spdlog::trace(format, type, data.pMessage);
                break;
            default:
                spdlog::error("Unexpected vulkan severity: {}, message[{}]: {}", severity, type,
                              data.pMessage);
                break;
            }

            return false;
        }
    };

    create_info.setPfnUserCallback(Logger::call);
}

} // namespace Vulkan