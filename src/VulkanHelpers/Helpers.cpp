#include "Helpers.hpp"

#include "DebugCallback.hpp"
#include "Formaters.hpp"

#include <spdlog/spdlog.h>
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_core.h>

namespace Vulkan {

vk::UniqueInstance createInstance(std::vector<const char*>&& extensions)
{
    vk::ApplicationInfo app_info{};
    app_info.pApplicationName   = "Vulkan Screw around";
    app_info.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
    app_info.pEngineName        = "No engine";
    app_info.engineVersion      = VK_MAKE_VERSION(0, 1, 0);
    app_info.apiVersion         = VK_API_VERSION_1_1;

    vk::InstanceCreateInfo create_info = {vk::InstanceCreateFlags{}, &app_info};

#ifdef VULKAN_VALIDATION_LAYERS
    const auto layers = std::array{"VK_LAYER_KHRONOS_validation"};
    create_info.setPEnabledLayerNames(layers);

    extensions.push_back("VK_EXT_debug_utils");

    using sevFlags  = vk::DebugUtilsMessageSeverityFlagBitsEXT;
    using typeFlags = vk::DebugUtilsMessageTypeFlagBitsEXT;

    vk::DebugUtilsMessengerCreateInfoEXT debug_create_info{
        {},
        sevFlags::eInfo | sevFlags::eError | sevFlags::eWarning,
        typeFlags::eGeneral | typeFlags::ePerformance | typeFlags::eValidation};

    set_debug_logger(debug_create_info);

    create_info.setPNext(static_cast<const void*>(&debug_create_info));
#endif
    create_info.setPEnabledExtensionNames(extensions);

    return vk::createInstanceUnique(create_info);
}

void logInstanceInfo(std::shared_ptr<spdlog::logger> logger)
{
    const VersionInfo vulkan_version{vk::enumerateInstanceVersion()};
    logger->info("Vulkan Instance version: {}", vulkan_version);

#ifdef VULKAN_VALIDATION_LAYERS
    const auto layers = vk::enumerateInstanceLayerProperties();
    logger->info("Available layers: {}", fmt::join(layers.begin(), layers.end(), ", "));
#endif

    const auto extensions = vk::enumerateInstanceExtensionProperties();
    logger->info("Supported extensions: {}", fmt::join(extensions.begin(), extensions.end(), ", "));
}

}