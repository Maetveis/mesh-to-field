#pragma once

#include <spdlog/fwd.h>
#include <vulkan/vulkan.hpp>

#include <memory>
#include <vector>

namespace Vulkan {

/**
 * \brief Create a Vulkan instance
 * \param extensions the vulkan extensions to enable
 * \note validation layers will be enabled if configured for the build
 */
vk::UniqueInstance createInstance(std::vector<const char*>&& extensions);

/**
 * \brief Log vulkan instance info
 * \param logger logger to log to
 */
void logInstanceInfo(std::shared_ptr<spdlog::logger> logger);

} // namespace Vulkan