#pragma once

#include <vulkan/vulkan.hpp>

namespace SDL {
class Window;
}

/**
 * \brief Initialize a vulkan instance and the required extensions
 */
vk::UniqueInstance setupInstance(SDL::Window& window);