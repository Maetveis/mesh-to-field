#include "Game.hpp"

#include <spdlog/spdlog.h>

int main(int argc, char* argv[])
{
    spdlog::set_level(spdlog::level::debug);
    Game game;
    game.run();
}