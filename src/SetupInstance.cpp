#include "SetupInstance.hpp"

#include "SdlWrapper/Vulkan.hpp"
#include "SdlWrapper/Window.hpp"
#include "VulkanHelpers/Helpers.hpp"

#include <SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

vk::UniqueInstance setupInstance(SDL::Window& window)
{
    VULKAN_HPP_DEFAULT_DISPATCHER.init(
        reinterpret_cast<PFN_vkGetInstanceProcAddr>(SDL_Vulkan_GetVkGetInstanceProcAddr()));

    auto instance = Vulkan::createInstance(SDL::vulkanRequiredExtensions(window));

    VULKAN_HPP_DEFAULT_DISPATCHER.init(instance.get());

    return instance;
}