#include "Game.hpp"

#include "SdlWrapper/Window.hpp"
#include "VulkanHelpers/DebugCallback.hpp"
#include "VulkanHelpers/Formaters.hpp"
#include "VulkanHelpers/FrameQueue.hpp"

#include <SDL_events.h>

#include <spdlog/spdlog.h>
#include <vulkan/vulkan.hpp>

#include <initializer_list>
#include <stdexcept>
#include <utility>
#include <vector>

namespace {

struct GameException : std::runtime_error {
    using runtime_error::runtime_error;
};

#ifdef VULKAN_VALIDATION_LAYERS
vk::DebugUtilsMessengerEXT createDebugMessenger(vk::Instance instance)
{
    using sevFlags  = vk::DebugUtilsMessageSeverityFlagBitsEXT;
    using typeFlags = vk::DebugUtilsMessageTypeFlagBitsEXT;

    vk::DebugUtilsMessengerCreateInfoEXT create_info{
        {},
        sevFlags::eInfo | sevFlags::eError | sevFlags::eWarning,
        typeFlags::eGeneral | typeFlags::ePerformance | typeFlags::eValidation};

    Vulkan::set_debug_logger(create_info);

    return instance.createDebugUtilsMessengerEXT(create_info);
}
#endif

} // unnamed namespace

class Game::SetupParams {
public:
    void setup(vk::Instance instance, SDL::Window& window)
    {
        surface_ = window.createVkSurface(instance);

        for (const auto& device : instance.enumeratePhysicalDevices()) {
            const auto result = device.getProperties2<vk::PhysicalDeviceProperties2,
                                                      vk::PhysicalDeviceIDProperties>();

            const auto& properties    = result.get<vk::PhysicalDeviceProperties2>().properties;
            const auto& id_properties = result.get<vk::PhysicalDeviceIDProperties>();

            spdlog::debug("Checking Vulkan device named: {}, device-id: {}, type: {}, api version: "
                          "{}, driver version: {}, device-uuid: {}",
                          properties.deviceName, properties.deviceID, properties.deviceType,
                          Vulkan::VersionInfo{properties.apiVersion},
                          Vulkan::VersionInfo{properties.driverVersion}, id_properties.deviceUUID);

            if (properties.apiVersion < VK_MAKE_VERSION(1, 1, 0)) {
                spdlog::debug("{}: Unsported api version: {}", properties.deviceName,
                              Vulkan::VersionInfo{properties.apiVersion});
                continue;
            }

            if (const auto support = Vulkan::FrameQueue::getPresentSupport(device, surface_.get()))
            {
                spdlog::info("Found suitable device: {}, device-id: {}, api version: {}",
                             properties.deviceName, properties.deviceID,
                             Vulkan::VersionInfo{properties.apiVersion});

                phy_device_          = device;
                presentation_params_ = support.value();
                return;
            }
        }
        throw GameException{"No suitable physical device found!"};
    }

    vk::UniqueDevice createDevice()
    {
        const auto queue_priorities   = {0.0f};
        const auto queue_create_infos = {vk::DeviceQueueCreateInfo{
            {}, presentation_params_.graphics_present_family, queue_priorities}};
        const auto device_extensions  = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

        return phy_device_.createDeviceUnique({{}, queue_create_infos, {}, device_extensions});
    }

    Vulkan::FrameQueue createFrameQueue(vk::Device device)
    {
        return {device, std::move(surface_), presentation_params_};
    }

    Triangle createTriangle(vk::Device device, const Vulkan::FrameQueue& frame_queue)
    {
        return {device, frame_queue, presentation_params_};
    }

private:
    vk::PhysicalDevice     phy_device_;
    vk::UniqueSurfaceKHR   surface_;
    Vulkan::PresentSupport presentation_params_;
};

Game::Game()
    : Game{{}}
{
}

Game::Game(SetupParams params)
    :
#ifdef VULKAN_VALIDATION_LAYERS
    debug_messenger_{createDebugMessenger(instance_.get())}
    ,
#endif
    device_{(Vulkan::logInstanceInfo(spdlog::default_logger()),
             params.setup(instance_.get(), window_), params.createDevice())}
    , frame_queue_(params.createFrameQueue(device_.get()))
    , triangle_{params.createTriangle(device_.get(), frame_queue_)}
{
}

Game::~Game()
{
#ifdef VULKAN_VALIDATION_LAYERS
    instance_->destroyDebugUtilsMessengerEXT(debug_messenger_);
#endif
    device_->waitIdle();
}

void Game::run()
{
    while (running_) {
        handleEvents();
        update();
        draw();
    }
}

void Game::handleEvents()
{
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_AUDIODEVICEADDED:
        case SDL_AUDIODEVICEREMOVED:
            break;
        case SDL_CONTROLLERAXISMOTION:
            break;
        case SDL_CONTROLLERBUTTONDOWN:
        case SDL_CONTROLLERBUTTONUP:
            break;
        case SDL_CONTROLLERDEVICEADDED:
        case SDL_CONTROLLERDEVICEREMOVED:
        case SDL_CONTROLLERDEVICEREMAPPED:
            break;
        case SDL_DROPFILE:
        case SDL_DROPTEXT:
        case SDL_DROPBEGIN:
        case SDL_DROPCOMPLETE:
            break;
        case SDL_FINGERMOTION:
        case SDL_FINGERDOWN:
        case SDL_FINGERUP:
            break;
        case SDL_KEYDOWN:
        case SDL_KEYUP:
            break;
        case SDL_JOYAXISMOTION:
            break;
        case SDL_JOYBALLMOTION:
            break;
        case SDL_JOYHATMOTION:
            break;
        case SDL_JOYBUTTONDOWN:
        case SDL_JOYBUTTONUP:
            break;
        case SDL_JOYDEVICEADDED:
        case SDL_JOYDEVICEREMOVED:
            break;
        case SDL_MOUSEMOTION:
            break;
        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP:
            break;
        case SDL_MOUSEWHEEL:
            break;
        case SDL_QUIT:
            running_ = false;
            break;
        case SDL_USEREVENT:
            break;
        case SDL_WINDOWEVENT:
            break;
        default:
            break;
        }
    }
}

void Game::update() { }

void Game::draw()
{
    triangle_.draw(frame_queue_.startFrame());
    frame_queue_.submitFrame();
}