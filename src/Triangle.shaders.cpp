#include "Triangle.shaders.hpp"

decltype(vertex_shader_code) vertex_shader_code =
#include "main.vert.h"
    ;

decltype(fragment_shader_code) fragment_shader_code =
#include "main.frag.h"
    ;