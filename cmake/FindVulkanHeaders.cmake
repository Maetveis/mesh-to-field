cmake_minimum_required(VERSION 3.13)
cmake_policy(VERSION 3.13)

find_path(VulkanHeaders_INCLUDE_DIR "vulkan/vulkan.h" DOC "Location of the vulkan headers")

find_package_handle_standard_args(VulkanHeaders DEFAULT_MSG VulkanHeaders_INCLUDE_DIR)

if(${VulkanHeaders_FOUND})
    add_library(VulkanHeaders::VulkanHeaders INTERFACE IMPORTED)
    target_include_directories(VulkanHeaders::VulkanHeaders
        SYSTEM INTERFACE ${VulkanHeaders_INCLUDE_DIR})
endif()