cmake_minimum_required(VERSION 3.13)
cmake_policy(VERSION 3.13)

find_program(GLSLC glslc REQUIRED)

macro(add_spirv_shader SPV_SOURCE SPV_OUTPUT)
    set(options OPTIMIZE)
    set(args SHADER_STAGE OUTPUT_FORMAT ENVIRONMENT SPV_VERSION)
    set(multi_args INCLUDE_DIRECTORIES COMPILE_DEFINITIONS)

    cmake_parse_arguments(SPV "${options}" "${args}" "${multi_args}" ${ARGN})

    set(shader_stage "$<$<BOOL:${SPV_SHADER_STAGE}>:-fshader-stage=${SPV_SHADER_STAGE}>")
    set(format "$<$<BOOL:${SPV_OUTPUT_FORMAT}>:-mfmt=${SPV_OUTPUT_FORMAT}>")
    set(environment "$<$<BOOL:${SPV_ENVIRONMENT}>:--target-env=${SPV_ENVIRONMENT}>")
    set(spv_version "$<$<BOOL:${SPV_SPV_VERSION}>:--target-spv=${SPV_SPV_VERSION}>")
    set(include_directories "$<$<BOOL:$<JOIN:${SPV_INCLUDE_DIRECTORIES},;>>:-I$<JOIN:${SPV_INCLUDE_DIRECTORIES},;-I>>")
    set(compile_definitions "$<$<BOOL:$<JOIN:${SPV_COMPILE_DEFINITIONS},;>>:-D$<JOIN:${SPV_COMPILE_DEFINITIONS},;-D>>")

    file(RELATIVE_PATH source_path "${CMAKE_BINARY_DIR}" "${SPV_SOURCE}")
    file(RELATIVE_PATH output_path "${CMAKE_BINARY_DIR}" "${CMAKE_CURRENT_BINARY_DIR}/${SPV_OUTPUT}")

    if(${CMAKE_GENERATOR} MATCHES "Ninja")
        add_custom_command(
            OUTPUT ${SPV_OUTPUT}
            DEPENDS ${SPV_SOURCE}
            COMMAND
                ${GLSLC}
                    -MD -MF "${output_path}.d" -o "${output_path}" "${shader_stage}" "${format}" "${environment}"
                    "${spv_version}" "${include_directories}" "${compile_definitions}" "${source_path}"
            VERBATIM
            COMMAND_EXPAND_LISTS
            DEPFILE ${output_path}.d
            BYPRODUCTS ${SPV_OUTPUT}.d
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        )
    else()
        add_custom_command(
            OUTPUT ${SPV_OUTPUT}
            DEPENDS ${SPV_SOURCE}
            COMMAND
                ${GLSLC}
                    -o "${output_path}" "${shader_stage}" "${format}" "${environment}"
                    "${spv_version}" "${include_directories}" "${compile_definitions}" "${source_path}"
            VERBATIM
            COMMAND_EXPAND_LISTS
            IMPLICIT_DEPENDS CXX ${SPV_SOURCE}
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        )
    endif()
endmacro()
