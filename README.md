# Mesh to field

Efficient GPU transformation of triangle meshes to Signed Distance Fields.

## Building
Required Dependencies:
- [fmt](https://github.com/fmtlib/fmt)
- [SDL2](https://www.libsdl.org/index.php)
- [spdlog](https://github.com/gabime/spdlog)
- [shaderc](https://github.com/google/shaderc)
- Vulkan headers

This project uses cmake.